// Bai 1: Ngay, Thang, Nam

function tinhNgay(){
    var day = document.getElementById("day").value * 1; 
    var month = document.getElementById("month").value * 1 - 1; 
    var year = document.getElementById("year").value * 1; 
    console.log({day, month, year});
    var dateInput = new Date(year, month, day);
    var ngayTruoc = new Date(dateInput);
    var ngaySau = new Date(dateInput);
    ngayTruoc.setDate(dateInput.getDate() - 1);
    ngaySau.setDate(dateInput.getDate() + 1);
    ngayTruoc = ngayTruoc.toDateString().split(' ').slice(1).join(' ');
    ngaySau = ngaySau.toDateString().split(' ').slice(1).join(' ');
    document.getElementById("bai-1-result").innerHTML = `<p>Ngày Hôm Trước: ${ngayTruoc} <br> Ngày Hôm Sau: ${ngaySau} </p>`;
}

// Bai 2:
function inSoNgay(){
    var thang = document.getElementById("thang").value * 1;
    var nam = document.getElementById("nam").value * 1;
    console.log(thang);
    var isNamNhuan = false;
    var soNgay = 0;
    if (nam % 100 == 0){
        if (nam % 400 == 0){
            isNamNhuan = true;
        } else {
            isNamNhuan = false;
        }
    } else {
        if (nam % 4 == 0){
            isNamNhuan = true;
        } else {
            isNamNhuan = false;
        }
    }
    switch(thang){
        case 1:{
            soNgay = 31;
        } break;
        case 2:{
            if (isNamNhuan){
                soNgay = 29;
            } else {
                soNgay = 28;
            }
        } break;
        case 3:{
            soNgay = 31;
        } break;
        case 5:{
            soNgay = 31;
        } break;
        case 7:{
            soNgay = 31;
        } break;
        case 8:{
            soNgay = 31;
        } break;
        case 10:{
            soNgay = 31;
        } break;
        case 12:{
            soNgay = 31;
        } break; 
        default:
            soNgay = 30;    
    }
    console.log(isNamNhuan);
    console.log(soNgay);
    document.getElementById("bai-2-result").innerHTML = `<p>Tháng ${thang} năm ${nam} có ${soNgay} ngày.</p>`;
}

// Bai 3: Doc So Nguyen
function docSoNguyen(){
    var soNguyen = document.getElementById('so-nguyen').value * 1;
    var hangDonVi = soNguyen % 10;
    var hangChuc = Math.floor((soNguyen/10)%10);
    var hangTram = Math.floor((soNguyen/100)%10);
    function docHangTram(tram){
        doc = "";
        switch(tram){
            case 1: {
                doc = "một trăm";
            } break;
            case 2: {
                doc = "hai trăm";
            } break;
            case 3: {
                doc = "ba trăm";
            }break;
            case 4: {
                doc = "bốn trăm";
            }break;
            case 5: {
                doc = "năm trăm";
            }break;
            case 6: {
                doc = "sáu trăm";
            }break;
            case 7: {
                doc = "bảy trăm";
            }break;
            case 8: {
                doc = "tám trăm";
            }break;
            case 9: {
                doc = "chín trăm";
            }break;
            default:
                doc = "";
        }
        return doc;
    }
    function docHangChuc(chuc){
        doc = "";
        switch(chuc){
            case 0: {
                if(hangDonVi == 0){
                    doc = "";
                } else {
                    doc = "lẻ";
                }
            } break;
            case 1: {
                doc = "mười";
            } break;
            case 2: {
                doc = "hai mươi";
            } break;
            case 3: {
                doc = "ba mươi";
            }break;
            case 4: {
                doc = "bốn mươi";
            }break;
            case 5: {
                doc = "năm mươi";
            }break;
            case 6: {
                doc = "sáu mươi";
            }break;
            case 7: {
                doc = "bảy mươi";
            }break;
            case 8: {
                doc = "tám mươi";
            }break;
            case 9: {
                doc = "chín mươi";
            }break;
            default:
                doc = "";
        }
        return doc;
    }
    function docHangDonVi(dv){
        doc = "";
        switch(dv){
            case 0: {
                doc = "";
            } break;
            case 1: {
                if (hangChuc == 1){
                    doc = "một";
                } else {
                    doc = "mốt";
                }
            } break;
            case 2: {
                doc = "hai";
            } break;
            case 3: {
                doc = "ba";
            }break;
            case 4: {
                doc = "bốn";
            }break;
            case 5: {
                if (hangChuc == 0){
                    doc = "năm";
                } else {
                    doc = "lăm";
                }
            }break;
            case 6: {
                doc = "sáu";
            }break;
            case 7: {
                doc = "bảy";
            }break;
            case 8: {
                doc = "tám";
            }break;
            case 9: {
                doc = "chín";
            }break;
            default:
                doc = "";
        }
        return doc;
    }
    var tramDoc = docHangTram(hangTram);
    var chucDoc = docHangChuc(hangChuc);
    var donViDoc = docHangDonVi(hangDonVi);
    console.log(tramDoc, chucDoc, donViDoc);
    document.getElementById("bai-3-result").innerHTML =`<p>${tramDoc} ${chucDoc} ${donViDoc}</p>`
}

// Bai 4: Tinh Khoang Cach
function tinhKhoangCach(){
    var hs1 = document.getElementById("ten1").value;
    var hs2 = document.getElementById("ten2").value;
    var hs3 = document.getElementById("ten3").value;
    var x1 = document.getElementById("x1").value * 1;
    var y1 = document.getElementById("y1").value * 1;
    var x2 = document.getElementById("x2").value * 1;
    var y2 = document.getElementById("y2").value * 1;
    var x3 = document.getElementById("x3").value * 1;
    var y3 = document.getElementById("y3").value * 1;
    var xdh = document.getElementById("xdh").value * 1;
    var ydh = document.getElementById("ydh").value * 1;
    var d1 = Math.sqrt(Math.pow((x1 - xdh),2) + Math.pow((y1-ydh),2));
    var d2 = Math.sqrt(Math.pow((x2 - xdh),2) + Math.pow((y2-ydh),2));
    var d3 = Math.sqrt(Math.pow((x3 - xdh),2) + Math.pow((y3-ydh),2));
    if (d1 > d2 && d1 > d3){
        console.log(`Nha ${hs1} xa nhat.`);
        document.getElementById("bai-4-result").innerHTML = `<p>Nhà ${hs1} xa trường nhất. </p>`
    }
    if (d2 > d1 && d2 > d3){
        console.log(`Nha ${hs2} xa nhat.`);
        document.getElementById("bai-4-result").innerHTML = `<p>Nhà ${hs2} xa trường nhất. </p>`
    }
    if (d3 > d1 && d3 > d2){
        console.log(`Nha ${hs3} xa nhat.`);
        document.getElementById("bai-4-result").innerHTML = `<p>Nhà ${hs3} xa trường nhất. </p>`
    }
}